FROM openjdk:11
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=target/myproject-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} myproject-0.0.1-SNAPSHOT.jar
COPY run.sh .
RUN chmod +x run.sh
